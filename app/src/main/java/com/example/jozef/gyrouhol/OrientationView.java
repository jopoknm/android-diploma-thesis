package com.example.jozef.gyrouhol;

/**
 * Created by jozef on 13.04.2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by jozef on 19.03.2018.
 */

public class OrientationView extends View {

    Bitmap bitmap;
    private float currX = 200;
    private float currY = 200;
    private float mPreviousX;
    private float mPreviousY;
    private float rotation;
    private Paint mPaint;

    public OrientationView(Context context, Bitmap bitmap) {
        super(context);
        this.bitmap = bitmap;
        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(20);
    }

    public void setCurrX(float currX) {
        this.currX = currX;
    }

    public void setCurrY(float currY) {
        this.currY = currY;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        canvas.drawCircle(currX,currY,5,mPaint);
        canvas.save();
        // do my rotation
        canvas.rotate(rotation,currX,currY);

        canvas.drawBitmap( bitmap,currX-(bitmap.getHeight()/2),currY-(bitmap.getWidth()/2),null);
        canvas.restore();

    }

    public boolean onTouchEvent(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();
        updateRotation(x,y);
        mPreviousX = x;
        mPreviousY = y;
        invalidate();
        return true;
    }

    private void updateRotation(float x, float y) {

        double r = Math.atan2(x -(currX),(currY) - y);
        rotation = (int) Math.toDegrees(r);
    }

    public float getRotation(){
        return rotation;
    }
}
