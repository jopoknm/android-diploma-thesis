package com.example.jozef.gyrouhol;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class LoggingActivity extends AppCompatActivity implements SensorEventListener {
    private Thread thread;
    private File dataWrite;
    private CsvFileWriting writer;
    private ScheduledThreadPoolExecutor exec;
    private SensorManager mSensorManager;
    private Sensor mGyroSensor, mMagSensor, mAkcSensor;
    private ObjectHandlerNew mData;
    private ViewGroup graphHolder;
    private XYPlot plot, plotGyro, plotMag, plotImu, plotMarg;
    private List<Float> bufferAx, bufferAy, buffgx, buffgy, buffgz;
    private float axoffset, ayoffset, gxbias, gybias, gzbias;
    private CheckBox cha, chg, chm, chi, chmarg;
    private int interval;
    SimpleXYSeries seriesAccx;
    SimpleXYSeries seriesAccy;
    SimpleXYSeries seriesAccz;

    SimpleXYSeries sGyrox;
    SimpleXYSeries sGyroy;
    SimpleXYSeries sGyroz;

    SimpleXYSeries sMagx;
    SimpleXYSeries sMagy;
    SimpleXYSeries sMagz;

    SimpleXYSeries sImuRoll;
    SimpleXYSeries sImuPitch;
    SimpleXYSeries sImuYaw;

    SimpleXYSeries sMargRoll;
    SimpleXYSeries sMargPitch;
    SimpleXYSeries sMargYaw;
    private static final int HISTORY_SIZE = 500;

    private Timer AccGraph = new Timer();

    private float[] lin_Akc = new float[3];
    private float[] gravity = new float[3];
    private View accGraphLayout, gyroGraphLayout, magGraphLayout, imuGraphLayout, margGraphLayout;

    private MadgwickAHRS madgwickAHRS;
    private IMUfilter imfilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging);
        mData = new ObjectHandlerNew();

        graphHolder = (ViewGroup) findViewById(R.id.graph_holder);
        writer = new CsvFileWriting();

        SharedPreferences shp = PreferenceManager.getDefaultSharedPreferences(this);
        interval = Integer.parseInt(shp.getString("period_sensor", "20"));
        axoffset = Float.valueOf(shp.getString("axoffset",String.valueOf(0)));
        ayoffset = Float.valueOf(shp.getString("ayoffset",String.valueOf(0)));
        gxbias = Float.valueOf(shp.getString("gxbias",String.valueOf(0)));
        gybias = Float.valueOf(shp.getString("gybias",String.valueOf(0)));
        gzbias = Float.valueOf(shp.getString("gzbias",String.valueOf(0)));

        cha = (CheckBox) findViewById(R.id.checkBox);
        chg = (CheckBox) findViewById(R.id.checkBox2);
        chm = (CheckBox) findViewById(R.id.checkBox3);
        chi = (CheckBox) findViewById(R.id.checkBox4);
        chmarg = (CheckBox) findViewById(R.id.checkBox5);

        cha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cha.isChecked()) {
                    setPlots();
                    AccGraph.scheduleAtFixedRate(new DoGraphAcc(),1000, 30);
                } else {
                    graphHolder.removeView(accGraphLayout);
                }
            }
        });
        chg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chg.isChecked()) {
                    setPlotsGyro();
                    AccGraph.scheduleAtFixedRate(new DoGraphGyro(),1000, 30);
                } else {
                    graphHolder.removeView(gyroGraphLayout);
                }
            }
        });
        chm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chm.isChecked()) {
                    setPlotsMag();
                    AccGraph.scheduleAtFixedRate(new DoGraphMag(),1000, 30);
                } else {
                    graphHolder.removeView(magGraphLayout);
                }
            }
        });
        chi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chi.isChecked()) {
                    float []q = {1,0,0,0};
                    imfilter = new IMUfilter(0.03f,q);
                    setPlotImu();
                    AccGraph.scheduleAtFixedRate(new DoGraphIMU(),1000, 30);
                } else {
                    graphHolder.removeView(imuGraphLayout);
                }
            }
        });
        chmarg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chmarg.isChecked()) {
                    madgwickAHRS = new MadgwickAHRS(0.03f, 0.5f);
                    setPlotMarg();
                    AccGraph.scheduleAtFixedRate(new DoGraphMarg(),1000, 30);
                } else {
                    graphHolder.removeView(margGraphLayout);
                }
            }
        });

        try {
            mSensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
            mMagSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mAkcSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

            mSensorManager.registerListener(this, mAkcSensor, SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mMagSensor, SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mGyroSensor, SensorManager.SENSOR_DELAY_FASTEST);
        } catch (Exception e) {
            Toast.makeText(this, "Hardware compatibility issue", Toast.LENGTH_LONG).show();
        }
    }


    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor == mAkcSensor) {

            mData.setxa(event.values[0] - axoffset);
            mData.setya(event.values[1] -ayoffset);
            mData.setza(event.values[2]);
        }

        if (event.sensor == mGyroSensor) {
            mData.setxg(event.values[0] -gxbias);
            mData.setyg(event.values[1]-gybias);
            mData.setzg(event.values[2]-gzbias);
        }

        if (event.sensor == mMagSensor) {
            mData.setxm(event.values[0]);
            mData.setym(event.values[1]);
            mData.setzm(event.values[2]);
        }
    }


    public void onClickStart(View view) {

        String deviceName = Build.MODEL;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        final String formattedDate = dateFormat.format(calendar.getTime());
        dataWrite = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/results", deviceName + "_" + formattedDate + ".csv");

        String[] entry = new String[]{"Gyroscope x  y z rad/s Acc x y z m/s2 Mag x y z T" + "\n"};
        writer.doWriting(dataWrite,entry);

        thread = new Thread() {
            @Override
            public void run() {
                String[] entry2 = new String[]{mData.getxa()+";"+mData.getya()+";"+mData.getza()+";"+mData.getxg() + ";"+mData.getyg() + ";"+mData.getzg() + ";"+mData.getxm() + ";"+mData.getym() + ";"+mData.getzm() + ";" + String.valueOf(System.currentTimeMillis()) + ";" + "\n"};
                writer.doWriting(dataWrite,entry2);
            }
        };
        exec = new ScheduledThreadPoolExecutor(1);
        exec.scheduleAtFixedRate(thread, 0, interval, TimeUnit.MILLISECONDS);

        Toast.makeText(this, "Logging started", Toast.LENGTH_SHORT).show();
    }

    public void onClickStop(View view) {
        if (thread != null) {
            exec.remove(thread);
            thread.interrupt();
            exec.shutdown();
        }

        Toast.makeText(this, "Logging Stopped", Toast.LENGTH_SHORT).show();
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener((SensorEventListener) this);
    }

    private class DoGraphAcc extends TimerTask {

        public void run() {

            if (seriesAccx.size() > HISTORY_SIZE) {
                seriesAccx.removeFirst();
                seriesAccy.removeFirst();
                seriesAccz.removeFirst();
            }
            //add the latest history sample:
            seriesAccx.addLast(null, mData.getxa());
            seriesAccy.addLast(null, mData.getya());
            seriesAccz.addLast(null, mData.getza());

            plot.post(new Runnable() {
                public void run() {
                plot.redraw();
                }
            });
        }
    }

    private class DoGraphGyro extends TimerTask {

        public void run() {
            if (sGyrox.size() > HISTORY_SIZE) {
                sGyrox.removeFirst();
                sGyroy.removeFirst();
                sGyroz.removeFirst();
            }

            sGyrox.addLast(null, mData.getxg());
            sGyroy.addLast(null, mData.getyg());
            sGyroz.addLast(null, mData.getzg());
            plotGyro.post(new Runnable() {
                public void run() {
                    plotGyro.redraw();
                }
            });

        }

    }

    private class DoGraphMag extends TimerTask {

        public void run() {
            if (sMagx.size() > HISTORY_SIZE) {
                sMagx.removeFirst();
                sMagy.removeFirst();
                sMagz.removeFirst();
            }

            sMagx.addLast(null, mData.getxm());
            sMagy.addLast(null, mData.getym());
            sMagz.addLast(null, mData.getzm());

            plotMag.post(new Runnable() {
                public void run() {
                    plotMag.redraw();
                }
            });


        }

    }

    private class DoGraphMarg extends TimerTask {

        public void run() {
            madgwickAHRS.Update(mData.getxg(),mData.getyg(),mData.getzg(),mData.getxa(),mData.getya(),mData.getza(),mData.getxm(),mData.getym(),mData.getzm());
            if (sMargRoll.size() > HISTORY_SIZE) {
                sMargRoll.removeFirst();
                sMargPitch.removeFirst();
                sMargYaw.removeFirst();
            }

            //add the latest history sample:
            sMargRoll.addLast(null, madgwickAHRS.MadgRoll);
            sMargPitch.addLast(null, madgwickAHRS.MadgPitch);
            sMargYaw.addLast(null, madgwickAHRS.MadgYaw);
            plotMarg.post(new Runnable() {
                public void run() {
                    plotMarg.redraw();
             }
             });


        }

    }

    private class DoGraphIMU extends TimerTask {

        public void run() {
            imfilter.filterUpdate(mData.getxg(),mData.getyg(),mData.getzg(),mData.getxa(),(mData.getya()),1);
            if (sImuRoll.size() > HISTORY_SIZE) {
                sImuRoll.removeFirst();
                sImuPitch.removeFirst();
                sImuYaw.removeFirst();
            }

            //add the latest history sample:
            sImuRoll.addLast(null,imfilter.roll );
            sImuPitch.addLast(null, imfilter.pitch);
            sImuYaw.addLast(null, imfilter.yaw);

            plotImu.post(new Runnable() {
                public void run() {
                    plotImu.redraw();
                }
            });

        }

    }

    public void setPlots() {
        accGraphLayout = LayoutInflater.from(this).inflate(R.layout.acc_graph, graphHolder, false);

        graphHolder.addView(accGraphLayout);

        plot = (XYPlot) accGraphLayout.findViewById(R.id.mySimpleXYPlotAcc);

        plot.setDomainStepValue(5);
        plot.setTicksPerRangeLabel(3);
        plot.setRangeLabel((Html.fromHtml("m/s<sup>2</sup>")).toString());
        plot.setRangeBoundaries(-15, 15, BoundaryMode.AUTO);
        plot.setDomainBoundaries(0, 500, BoundaryMode.FIXED);
        seriesAccx = new SimpleXYSeries("accx");
        seriesAccx.useImplicitXVals();
        plot.addSeries(seriesAccx, new LineAndPointFormatter(
                Color.rgb(100, 100, 0), null, null, null));
        seriesAccy = new SimpleXYSeries("accy");
        seriesAccy.useImplicitXVals();
        plot.addSeries(seriesAccy, new LineAndPointFormatter(
                Color.rgb(0, 100, 200), null, null, null));
        seriesAccz = new SimpleXYSeries("accz");
        seriesAccz.useImplicitXVals();
        plot.addSeries(seriesAccz, new LineAndPointFormatter(
                Color.rgb(100, 0, 200), null, null, null));
    }

    public void setPlotsGyro() {
        gyroGraphLayout = LayoutInflater.from(this).inflate(R.layout.gyro_graph, graphHolder, false);

        graphHolder.addView(gyroGraphLayout);

        plotGyro = (XYPlot) gyroGraphLayout.findViewById(R.id.xy_plot_gyro);

        plotGyro.setDomainStepValue(5);
        plotGyro.setTicksPerRangeLabel(3);
        plotGyro.setRangeLabel((Html.fromHtml("rad/s")).toString());
        plotGyro.setRangeBoundaries(-15, 15, BoundaryMode.AUTO);
        plotGyro.setDomainBoundaries(0, 500, BoundaryMode.FIXED);
        sGyrox = new SimpleXYSeries("gx");

        sGyrox.useImplicitXVals();
        plotGyro.addSeries(sGyrox, new LineAndPointFormatter(
                Color.rgb(100, 100, 0), null, null, null));
        sGyroy = new SimpleXYSeries("gy");
        sGyroy.useImplicitXVals();
        plotGyro.addSeries(sGyroy, new LineAndPointFormatter(
                Color.rgb(0, 100, 200), null, null, null));
        sGyroz = new SimpleXYSeries("gz");
        sGyroz.useImplicitXVals();
        plotGyro.addSeries(sGyroz, new LineAndPointFormatter(
                Color.rgb(100, 0, 200), null, null, null));
    }

    public void setPlotsMag() {
        magGraphLayout = LayoutInflater.from(this).inflate(R.layout.mag_graph, graphHolder, false);

        graphHolder.addView(magGraphLayout);

        plotMag = (XYPlot) magGraphLayout.findViewById(R.id.xy_plot_mag);

        plotMag.setDomainStepValue(5);
        plotMag.setTicksPerRangeLabel(3);
        plotMag.setRangeLabel((Html.fromHtml("μT")).toString());
        plotMag.setRangeBoundaries(-15, 15, BoundaryMode.AUTO);
        plotMag.setDomainBoundaries(0, 500, BoundaryMode.FIXED);

        sMagx = new SimpleXYSeries("mx");
        sMagx.useImplicitXVals();
        plotMag.addSeries(sMagx, new LineAndPointFormatter(
                Color.rgb(100, 100, 0), null, null, null));
        sMagy = new SimpleXYSeries("my");
        sMagy.useImplicitXVals();
        plotMag.addSeries(sMagy, new LineAndPointFormatter(
                Color.rgb(0, 100, 200), null, null, null));
        sMagz = new SimpleXYSeries("mz");
        sMagz.useImplicitXVals();
        plotMag.addSeries(sMagz, new LineAndPointFormatter(
                Color.rgb(100, 0, 200), null, null, null));
    }

    public void setPlotImu() {
        imuGraphLayout = LayoutInflater.from(this).inflate(R.layout.imu_angle_graph, graphHolder, false);

        graphHolder.addView(imuGraphLayout);

        plotImu = (XYPlot) imuGraphLayout.findViewById(R.id.xy_plot_imu);

        plotImu.setDomainStepValue(5);
        plotImu.setTicksPerRangeLabel(3);
        plotImu.setRangeLabel((Html.fromHtml("rad")).toString());
        plotImu.setRangeBoundaries(-15, 15, BoundaryMode.AUTO);
        plotImu.setDomainBoundaries(0, 500, BoundaryMode.FIXED);

        sImuRoll = new SimpleXYSeries("roll");
        sImuRoll.useImplicitXVals();
        plotImu.addSeries(sImuRoll, new LineAndPointFormatter(
                Color.rgb(100, 100, 0), null, null, null));
        sImuPitch = new SimpleXYSeries("pitch");
        sImuPitch.useImplicitXVals();
        plotImu.addSeries(sImuPitch, new LineAndPointFormatter(
                Color.rgb(0, 100, 200), null, null, null));
        sImuYaw = new SimpleXYSeries("yaw");
        sImuYaw.useImplicitXVals();
        plotImu.addSeries(sImuYaw, new LineAndPointFormatter(
                Color.rgb(100, 0, 200), null, null, null));
    }

    public void setPlotMarg() {
        margGraphLayout = LayoutInflater.from(this).inflate(R.layout.marg_angle_graph, graphHolder, false);

        graphHolder.addView(margGraphLayout);

        plotMarg = (XYPlot) margGraphLayout.findViewById(R.id.xy_plot_marg);

        plotMarg.setDomainStepValue(5);
        plotMarg.setTicksPerRangeLabel(3);
        plotMarg.setRangeLabel((Html.fromHtml("rad")).toString());
        plotMarg.setRangeBoundaries(-15, 15, BoundaryMode.AUTO);
        plotMarg.setDomainBoundaries(0, 500, BoundaryMode.FIXED);

        sMargRoll = new SimpleXYSeries("roll");
        sMargRoll.useImplicitXVals();
        plotMarg.addSeries(sMargRoll, new LineAndPointFormatter(
                Color.rgb(100, 100, 0), null, null, null));
        sMargPitch = new SimpleXYSeries("pitch");
        sMargPitch.useImplicitXVals();
        plotMarg.addSeries(sMargPitch, new LineAndPointFormatter(
                Color.rgb(0, 100, 200), null, null, null));
        sMargYaw = new SimpleXYSeries("yaw");
        sMargYaw.useImplicitXVals();
        plotMarg.addSeries(sMargYaw, new LineAndPointFormatter(
                Color.rgb(100, 0, 200), null, null, null));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    public void startCalibration(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoggingActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.progres_bar_dialog_layout, null);
        final Button okButton = (Button) mView.findViewById(R.id.ok_button);
        final Button startButton = (Button) mView.findViewById(R.id.start_button);
        final TextView tw = (TextView) mView.findViewById(R.id.calibration_textView) ;

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        okButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                dialog.dismiss();
            }
        });

        startButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startButton.setEnabled(false);
                okButton.setEnabled(true);

                bufferAx = new ArrayList<>();
                bufferAy = new ArrayList<>();
                buffgx = new ArrayList<>();
                buffgy = new ArrayList<>();
                buffgz = new ArrayList<>();


                tw.setVisibility(View.INVISIBLE);

                Thread threadCalibration = new Thread() {
                    @Override
                    public void run() {
                        bufferAx.add(mData.getxa());
                        bufferAy.add(mData.getya());
                        buffgx.add(mData.getxg());
                        buffgy.add(mData.getyg());
                        buffgz.add(mData.getzg());
                    }
                };
                exec = new ScheduledThreadPoolExecutor(1);
                exec.scheduleAtFixedRate(threadCalibration, 0, 30, TimeUnit.MILLISECONDS);

                final ProgressDialog progress = new ProgressDialog(LoggingActivity.this);
                progress.setTitle("Calibration");
                progress.setMessage("Please wait while we calibrate the sensors");
                progress.show();
                Runnable progressRunnable = new Runnable() {

                    @Override
                    public void run() {

                        progress.cancel();
                        exec.shutdown();
                        float sum =0;
                        float sum2 =0;
                        float sum3 =0;
                        float sum4 =0;
                        float sum5 =0;

                        for (int i =0;i< bufferAx.size();i++){
                            sum += bufferAx.get(i);
                            sum2 += bufferAy.get(i);
                            sum3 += buffgx.get(i);
                            sum4 += buffgy.get(i);
                            sum5 += buffgz.get(i);
                        }
                        axoffset = sum / bufferAx.size();
                        ayoffset = sum2 / bufferAy.size();
                        gxbias = sum3 / buffgx.size();
                        gybias = sum4 / buffgy.size();
                        gzbias = sum5 / buffgz.size();

                        PreferenceManager.getDefaultSharedPreferences(LoggingActivity.this).edit().putString("axoffset", Float.toString(axoffset)).apply();
                        PreferenceManager.getDefaultSharedPreferences(LoggingActivity.this).edit().putString("ayoffset", Float.toString(ayoffset)).apply();
                        PreferenceManager.getDefaultSharedPreferences(LoggingActivity.this).edit().putString("gxbias", Float.toString(gxbias)).apply();
                        PreferenceManager.getDefaultSharedPreferences(LoggingActivity.this).edit().putString("gybias", Float.toString(gybias)).apply();
                        PreferenceManager.getDefaultSharedPreferences(LoggingActivity.this).edit().putString("gzbias", Float.toString(gzbias)).apply();

                        tw.setVisibility(View.VISIBLE);
                        tw.setTextSize(20);
                        tw.setText("Calibration Done!");
                    }
                };

                android.os.Handler pdCanceller = new android.os.Handler();
                pdCanceller.postDelayed(progressRunnable, 20000);
            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.action_calibration){
            startCalibration();
        }
        return super.onOptionsItemSelected(item);
    }

}
