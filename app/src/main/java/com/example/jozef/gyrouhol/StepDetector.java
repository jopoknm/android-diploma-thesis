package com.example.jozef.gyrouhol;

import com.opencsv.CSVWriter;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jozef on 01.03.2018.
 */

public class StepDetector  {

    private boolean pmax = false, pmin = false;
    private int count;
    private  long actualTime;
    private Set<StepListener> listeners;
    private float minconst,maxconst;

    public StepDetector(){
        listeners = new HashSet<>();
    }

    public void setMinMax( float maxconst, float minconst){
        this.minconst = minconst;
        this.maxconst = maxconst;
    }

    public void stepCount(double mNorming) {
        if (mNorming > maxconst)
            pmax = true;

        if (mNorming < minconst)
            pmin = true;

        if (pmax  && pmin ) {

            if (count == 0) {
                count++;
                actualTime = System.currentTimeMillis();
                fireStepEvent();

            } else {
                if (System.currentTimeMillis() - actualTime > 560) {
                    count++;
                    actualTime = System.currentTimeMillis();
                    fireStepEvent();
                }
            }
            pmin = false;
            pmax = false;
        }
    }

    public synchronized void addStepListener(StepListener listener){
        listeners.add(listener);
    }

    public synchronized void removeStepListener(StepListener listener){
        listeners.remove(listener);
    }

    protected synchronized void fireStepEvent(){
        StepEvent e = new StepEvent(this);
        for(StepListener listener : listeners) {
            listener.stepDetected(e);
        }
    }
}
