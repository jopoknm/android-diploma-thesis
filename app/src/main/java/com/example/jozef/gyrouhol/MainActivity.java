package com.example.jozef.gyrouhol;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.Manifest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {

    private static Button butt_test, butt_school_test, github;
    private static final int RESULT_PERMS_INITIAL = 20;

    public void OnClickButtonListenerTest() {
        butt_test = (Button) findViewById(R.id.testbutton);
        butt_test.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HouseMap.class);
                startActivity(intent);
            }
        });
    }

    public void OnClicktestSchoolStartActivity() {
        butt_school_test = (Button) findViewById(R.id.testSchoolButton);
        butt_school_test.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SchoolMap.class);
                startActivity(intent);
            }
        });
    }

    public void aboutUsStartActivity() {
        Intent i = new Intent(MainActivity.this, AboutUs.class);
        startActivity(i);
    }


    public void settingStartActivity() {
        Intent i = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(i);
    }


    public void onClickStartLoggActivity(View view) {
        Intent i = new Intent(MainActivity.this, LoggingActivity.class);
        startActivity(i);
    }

    private boolean checkAndRequestPermissions() {

        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
            return false;
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        OnClickButtonListenerTest();
        OnClicktestSchoolStartActivity();
        checkAndRequestPermissions();
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, RESULT_PERMS_INITIAL);
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "results");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("App", "failed to create directory");
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        menu.removeItem(R.id.action_calibration);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            settingStartActivity();
            return true;
        }

        if (id == R.id.action_about_us) {
            aboutUsStartActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
