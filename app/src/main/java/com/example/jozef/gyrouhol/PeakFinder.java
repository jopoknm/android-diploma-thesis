package com.example.jozef.gyrouhol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by jozef on 25.02.2018.
 */

public class PeakFinder {

    private List<Float> buffer = new  ArrayList<Float>();

    public void addSample(float Sample){
        buffer.add(Sample);
    }

    public float[] findMaxfindMin()
    {
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        float zmax = Collections.max(buffer);
        float zmin = Collections.min(buffer);

        float[] minmax = new float[]{zmax, zmin};
        buffer.clear();
        return minmax;

    }

}
