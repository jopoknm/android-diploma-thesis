package com.example.jozef.gyrouhol;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by jozef on 19.09.2018.
 */

public class CsvFileWriting {

    private CSVWriter writer;

    public CsvFileWriting(){

    }

    public void doWriting(File file, String[] entry){
        try {
            writer = new CSVWriter(new FileWriter(file, true), Character.MIN_VALUE, Character.MIN_VALUE, "");
            writer.writeNext(entry);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public void doWriting(File file, ArrayList<String[]> entry){
        try {
            writer = new CSVWriter(new FileWriter(file, true), Character.MIN_VALUE, Character.MIN_VALUE, "");
            writer.writeAll(entry);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
