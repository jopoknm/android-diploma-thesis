package com.example.jozef.gyrouhol;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.bitmap;
import static android.R.attr.height;
import static android.R.attr.homeAsUpIndicator;
import static android.R.attr.x;
import static android.R.attr.y;
import static android.graphics.PathMeasure.POSITION_MATRIX_FLAG;
import static com.example.jozef.gyrouhol.R.color.colorReferencePath;

/**
 * Created by jozef on 28.11.2017.
 */

public class HouseBackground extends View {

    private Bitmap house;
    private Bitmap mScaledBackground;
    private Paint mPaint, mPaintb;
    private int mWidth;
    private int mHeight;
    private float mScaleFactor = 1.f;
    private final static float mMinZoom = 1f;
    private final static float mMaxZoom = 5f;
    private ScaleGestureDetector mScaleDetector;
    private final static int NONE = 0;
    private final static int PAN = 1;
    private final static int ZOOM = 2;
    private int mLastX, mLastY;
    private int mEventState;
    private float mStartX = 0f;
    private float mStartY = 0f;
    private float mTranslateX = 0f;
    private float mTranslateY = 0f;
    private float mPreviousTranslateX = 0f;
    private float mPreviousTranslateY = 0f;
    private ArrayList<Point> mPoints = new ArrayList<Point>();

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            mScaleFactor = Math.max(mMinZoom, Math.min(mMaxZoom, mScaleFactor));
            return true;
        }
    }

    public HouseBackground(Context context) {
        super(context);
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mHeight = size.y;
        mWidth = size.x;

        mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        house = BitmapFactory.decodeResource(getResources(), R.drawable.plochanew);

        Bitmap scaled = Bitmap.createScaledBitmap(house, mWidth, mHeight, true);
        mScaledBackground = scaled;

        mLastX = mScaledBackground.getHeight() - 20;
        mPoints.add(new Point(mLastX, 480));

        setupmPaint();
    }

    public HouseBackground(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        mHeight = size.y;
        mWidth = size.x;

        mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        house = BitmapFactory.decodeResource(getResources(), R.drawable.ref_point);

        Bitmap scaled = Bitmap.createScaledBitmap(house, 720, 1220, true);
        mScaledBackground = scaled;

        // Initial point
        mPoints.add(new Point(787, 483));
        mLastX = 787;
        mLastY = 483;

        setupmPaint();
    }

    public HouseBackground(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mHeight = size.y;
        mWidth = size.x;

        mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        house = BitmapFactory.decodeResource(getResources(), R.drawable.plochanew);

        mScaledBackground = house;

        mLastX = mScaledBackground.getHeight() - 20;
        mPoints.add(new Point(mLastX, 50));

        setupmPaint();
    }

    public boolean onTouchEvent(MotionEvent ev) {
        // Let the ScaleGestureDetector inspect all events.

        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mEventState = PAN;
                mStartX = ev.getX() - mPreviousTranslateX;
                mStartY = ev.getY() - mPreviousTranslateY;
                break;
            case MotionEvent.ACTION_UP:
                mEventState = NONE;
                mPreviousTranslateX = mTranslateX;
                mPreviousTranslateY = mTranslateY;
                break;
            case MotionEvent.ACTION_MOVE:
                mTranslateX = ev.getX() - mStartX;
                mTranslateY = ev.getY() - mStartY;

                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                mEventState = ZOOM;
                break;
        }
        mScaleDetector.onTouchEvent(ev);
        if ((mEventState == PAN) || mEventState == ZOOM) {
            invalidate();
            requestLayout();
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();

        // for bitmap moving
        canvas.scale(mScaleFactor, mScaleFactor);
        if ((mTranslateX * -1) < 0) {
            mTranslateX = 0;
        } else if ((mTranslateX * -1) > mWidth * mScaleFactor - getWidth()) {
            mTranslateX = (mWidth * mScaleFactor - getWidth()) * -1;
        }
        if ((mTranslateY * -1) < 0) {
            mTranslateY = 0;
        } else if ((mTranslateY * -1) > mHeight * mScaleFactor - getHeight()) {
            mTranslateY = (mHeight * mScaleFactor - getHeight()) * -1;
        }
        canvas.translate(mTranslateX / mScaleFactor, mTranslateY / mScaleFactor);
        canvas.drawBitmap(mScaledBackground, 0, 0, null);

        // draw points
        if (mPoints.size() > 1) {
            for (int i = 1; i < mPoints.size(); i++) {
                if (mPoints.size() > 2 && i > 1) {
                    if (mPoints.get(i).x > mPoints.get(i - 1).x) {
                        canvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 5, mPaintb);
                        canvas.drawLine(mPoints.get(i - 1).y, mPoints.get(i - 1).x, mPoints.get(i).y, mPoints.get(i).x, mPaintb);
                    }

                    if (mPoints.get(i).x < mPoints.get(i - 1).x) {
                        canvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 5, mPaint);
                        canvas.drawLine(mPoints.get(i - 1).y, mPoints.get(i - 1).x, mPoints.get(i).y, mPoints.get(i).x, mPaint);
                    }

                } else {
                    canvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 5, mPaint);
                    canvas.drawLine(mPoints.get(i - 1).y, mPoints.get(i - 1).x, mPoints.get(i).y, mPoints.get(i).x, mPaint);
                }
                mLastX = (mLastX - mPoints.get(i).x);
                mLastY =  (mLastY - mPoints.get(i).y);

            }
        }
        canvas.drawCircle(mPoints.get(0).y, mPoints.get(0).x, 5, mPaint);

        mLastX = mPoints.get(mPoints.size() - 1).x;
        mLastY = mPoints.get(mPoints.size() - 1).y;

        canvas.restore();
    }

    public void newPointAdd(int mX, int mY) {
        mPoints.add(new Point(mX, mY));
        invalidate();
    }

    public int getLastX() {
        return mLastX;
    }

    public int getLastY() {
        return mLastY;
    }

    public void setupmPaint() {
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setStrokeWidth(5);

        mPaintb = new Paint();
        mPaintb.setColor(Color.BLUE);
        mPaintb.setStrokeWidth(5);

    }
}
