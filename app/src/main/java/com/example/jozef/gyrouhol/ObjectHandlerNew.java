package com.example.jozef.gyrouhol;

/**
 * Created by jozef on 16.02.2018.
 */

public class ObjectHandlerNew {
    private float xa,ya,za,xg,yg,zg,xm,ym,zm,tt;
    private float[] akcNonFiltered;

    public void setxa(float xa) {
        this.xa = xa;
    }
    public void setya(float ya) {
        this.ya = ya;
    }
    public void setza(float za) {
        this.za = za;
    }

    public void setxg(float xg) {
        this.xg = xg;
    }
    public void setyg(float yg) {
        this.yg = yg;
    }
    public void setzg(float zg) {
        this.zg = zg;
    }

    public void setxm(float xm) {
        this.xm = xm;
    }
    public void setym(float ym) {
        this.ym = ym;
    }
    public void setzm(float zm) {
        this.zm = zm;
    }

    public void setAkcNonFiltered(float []data){
        akcNonFiltered = data;
    }

    public float[] getAkcNonFiltered(){
        return  akcNonFiltered;
    }
    public float getxa(){
        return xa;
    }
    public float getxg(){
        return xg;
    }
    public float getxm(){
        return xm;
    }

    public float getya(){
        return ya;
    }
    public float getyg(){
        return yg;
    }
    public float getym(){
        return ym;
    }

    public float getza(){
        return za;
    }
    public float getzg(){
        return zg;
    }
    public float getzm(){
        return zm;
    }

    public double gettt(){
        return tt;
    }
}
