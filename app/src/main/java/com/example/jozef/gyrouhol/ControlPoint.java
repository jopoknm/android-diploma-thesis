package com.example.jozef.gyrouhol;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by jozef on 17.04.2018.
 */

public class ControlPoint {
    private ArrayList<String[]> input;
    private File filename;
    private CSVWriter writer;

    public ControlPoint(){
        input = new ArrayList<>();
    }
    public void addLine(String[] line){
        input.add(line);
    }

    public void addFile(File filename){
        this.filename = filename;
    }

    public ArrayList<String[]> getInput(){
        return  input;
    }

    public void writeAll(){
        CsvFileWriting csvWriter = new CsvFileWriting();
        csvWriter.doWriting(filename,input);
    }
}
