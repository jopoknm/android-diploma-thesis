package com.example.jozef.gyrouhol;

public class DataWorker {

    private static final float ALPHA = 0.1f;
    public DataWorker()  {

    }

    public  float[] doLowPassFiltering(float[] input, float[] output ) {
        if ( output == null ) return input;
        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    }

    public double getStepLength(float max, float min){
        float stepLenght = (float)Math.pow((max-min),0.25f);
        return stepLenght;
    }

}
