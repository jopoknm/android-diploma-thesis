package com.example.jozef.gyrouhol;

/**
 * Created by jozef on 01.03.2018.
 */

public class StepEvent {

    private StepDetector source;

    public StepEvent(StepDetector source){
        this.source = source;
    }

    public StepDetector getSource(){
        return source;
    }

}
