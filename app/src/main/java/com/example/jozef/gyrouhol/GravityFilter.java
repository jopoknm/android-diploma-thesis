package com.example.jozef.gyrouhol;



public class GravityFilter {

    private float[] gravity =  { 0f, 0f, 0f };
    private float alpha = 0.1f;
    private float[] linearAcceleration =  { 0f, 0f, 0f };

    public float[] gravityFilter(float[] acceleration)
    {

        gravity[0] = alpha * gravity[0] + (1 - alpha) * acceleration[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * acceleration[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * acceleration[2];

        linearAcceleration[0] = acceleration[0] - gravity[0];
        linearAcceleration[1] = acceleration[1] - gravity[1];
        linearAcceleration[2] = acceleration[2] - gravity[2];

        return linearAcceleration;
    }
}
