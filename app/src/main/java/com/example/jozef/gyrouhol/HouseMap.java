package com.example.jozef.gyrouhol;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import java.io.FileOutputStream;
import java.lang.Math;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import com.opencsv.CSVWriter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;


import static com.example.jozef.gyrouhol.R.id.xxx;


public class HouseMap extends AppCompatActivity implements SensorEventListener, StepListener {

    private float gravSensorVals[];
    private SensorManager mSensorManager;
    private Sensor mMagSensor, mAkcSensor, mGyroSensor;

    private ObjectHandlerNew mData;
    private ScheduledThreadPoolExecutor exec;

    private HouseBackground myView;

    private Button undo, cntrPointButton;
    private TextView sctw;

    private int count =0;
    private GravityFilter gravityFilter;
    private PeakFinder peakFinder;
    private IMUfilter imfilter;
    private MadgwickAHRS madgwickAHRS;
    private double [] angles = {0f, 0f ,0f, 0f};;
    private float[] linearacc = {0f, 0f ,0f};
    private double angle;
    private StepDetector stepDetector;
    private ControlPoint controlPoint;

    private boolean dataLogin, stepLogin;
    private float axoffset;
    private float ayoffset;
    private float gxbias;
    private float gybias;
    private float gzbias;
    private int stepSize;
    private int filterType;

    private File dataWrite, stepWrite;
    private CsvFileWriting csvWriter;
    private String formattedDate;
    private float nonfiltered[] = {0,0,0};


    private DataWorker dataWorker;
    public void OnClickButtonListenerUndo()
    {
        undo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                controlPoint.addLine(new String[]{myView.getLastX() + ";" + myView.getLastY() + ";" + System.currentTimeMillis() + ";" + "\n"});
            }
        });
    }

    public void OnClickControlPointListener(){
        cntrPointButton.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                if(controlPoint.getInput()!= null){
                    controlPoint.addFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/results",  "_" + "control_points" +formattedDate + ".csv"));
                    controlPoint.writeAll();
                }
                saveDrawing();
            }
        });
    }


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gyro);
        sctw =(TextView)findViewById(R.id.stepsCountTextView);

        csvWriter = new CsvFileWriting();

        /** shared prefferences*/
        SharedPreferences shp = PreferenceManager.getDefaultSharedPreferences(this);
        filterType = Integer.parseInt(shp.getString("filter_type","1"));
        float pedometerMax = Float.valueOf(shp.getString("max_peak", String.valueOf(10.37)));
        float pedometerMin = Float.valueOf(shp.getString("min_peak", String.valueOf(8.5)));
        dataLogin = shp.getBoolean("switch_preference_data_login",false);
        stepLogin = shp.getBoolean("switch_preference_step_login",false);
        int sensorPeriod = Integer.parseInt(shp.getString("period_sensor", "20"));
        stepSize = Integer.parseInt(shp.getString("step_size","100"));
        axoffset = Float.valueOf(shp.getString("axoffset",String.valueOf(0)));
        ayoffset = Float.valueOf(shp.getString("ayoffset",String.valueOf(0)));
        gxbias = Float.valueOf(shp.getString("gxbias",String.valueOf(0)));
        gybias = Float.valueOf(shp.getString("gybias",String.valueOf(0)));
        gzbias = Float.valueOf(shp.getString("gzbias",String.valueOf(0)));

        sctw.setText(""+axoffset+";"+ayoffset+";"+gxbias+";"+gybias+";"+gzbias);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        switch(filterType){
            case 1:
                float []q = {1,0,0,0};
                imfilter = new IMUfilter(sensorPeriod *0.001f,q);
                break;
            case 2:
                madgwickAHRS = new MadgwickAHRS(0.005f, 0.5f);
                break;
        }

        myView = (com.example.jozef.gyrouhol.HouseBackground)findViewById(xxx);
        myView.setDrawingCacheEnabled(true);
        undo = (Button)findViewById(R.id.undo_butt);
        cntrPointButton =(Button)findViewById(R.id.control_point_butt);

        gravityFilter = new GravityFilter();
        peakFinder = new PeakFinder();

        mData = new ObjectHandlerNew();
        controlPoint = new ControlPoint();

        String deviceName = Build.MODEL;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat  df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        formattedDate = df.format(c.getTime());

        if(dataLogin){
            dataWrite = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/results", deviceName + "_" + formattedDate + ".csv");
        }

        if(stepLogin){
            stepWrite = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/results", deviceName + "_" + "points_" +formattedDate + ".csv");
        }

        stepDetector = new StepDetector();
        stepDetector.addStepListener(this);
        stepDetector.setMinMax(pedometerMax, pedometerMin);

        OnClickButtonListenerUndo();
        OnClickControlPointListener();

        try {
            mSensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);

            mMagSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mAkcSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

            mSensorManager.registerListener(this, mMagSensor, SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this,mAkcSensor,SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this,mGyroSensor,SensorManager.SENSOR_DELAY_FASTEST);
        }
        catch (Exception e) {
            Toast.makeText(this, "Hardware compatibility issue", Toast.LENGTH_LONG).show();
        }

        exec = new ScheduledThreadPoolExecutor(1);
        exec.scheduleAtFixedRate(new Runnable() {
            private Runnable update = new Runnable() {
                @Override
                public void run() {

                    if(mData.getAkcNonFiltered() != null){
                        nonfiltered = mData.getAkcNonFiltered();
                    }
                    if(filterType == 1){
                        imfilter.filterUpdate(mData.getxg(),mData.getyg(),mData.getzg(),mData.getxa(),(float)(mData.getya()),1);
                        angle =  imfilter.cptangle();
                    }
                    if(filterType == 2 ){
                        madgwickAHRS.Update(mData.getxg(),mData.getyg(),mData.getzg(),mData.getxa(),mData.getya(),mData.getza(),mData.getxm(),mData.getym(),mData.getzm());
                        angle = madgwickAHRS.MadgYaw;
                    }
                    stepDetector.stepCount(Math.sqrt((nonfiltered[0]*nonfiltered[0])+nonfiltered[1]*nonfiltered[1]+nonfiltered[2]*nonfiltered[2]));
                    peakFinder.addSample(linearacc[2]);

                    if(dataLogin){
                        String[] entry = new String[]{ mData.getxg()+";" +mData.getyg()+";"+mData.getzg()+";"+ nonfiltered[0]+";" +nonfiltered[1]+";"+nonfiltered[2]+";"+ mData.getxm()+";" +mData.getym()+";"+mData.getzm()+";"+angles[0]+";"+angle+";"+String.valueOf(System.currentTimeMillis()) + ";" + "\n"};
                        csvWriter.doWriting(dataWrite,entry);
                    }
                }
            };
            @Override
            public void run() {
                runOnUiThread(update);
            }
        }, 0, sensorPeriod, TimeUnit.MILLISECONDS);

        dataWorker = new DataWorker();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
    }

    public void stepDetected(StepEvent e) {
        count++;
        if (filterType == 1) {
            angle =  imfilter.cptangle();
        }

        if(filterType == 2){
            angle = madgwickAHRS.MadgYaw;
        }
        float[] minmax = peakFinder.findMaxfindMin();
        double step = (int) Math.round(dataWorker.getStepLength(minmax[0],minmax[1])*stepSize);
        sctw.setText(""+step/ stepSize);
        myView.newPointAdd((int) (myView.getLastX()-Math.round((step*Math.cos(angle)) )), (int) (myView.getLastY()-Math.round((step*Math.sin(angle)))));

        if(stepLogin){
            String[] entry = new String[]{ myView.getLastX()+";"+ myView.getLastY()+";"+count+";"+angle+";"+String.valueOf(System.currentTimeMillis()) + ";" + "\n"};
            csvWriter.doWriting(stepWrite,entry);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor == mMagSensor) {
            mData.setxm((float)(event.values[0]*0.01));
            mData.setym((float)(event.values[1]*0.01));
            mData.setzm((float)(event.values[2]*0.01));
        }

        if(event.sensor == mGyroSensor) {

            mData.setxg((event.values[0] - gxbias));
            mData.setyg((event.values[1] - gybias));
            mData.setzg((event.values[2] - gzbias));
        }

        if(event.sensor == mAkcSensor) {
            event.values[0] = (event.values[0] - ayoffset);
            event.values[1] = (event.values[1] - axoffset);
            gravSensorVals = dataWorker.doLowPassFiltering(event.values.clone(), gravSensorVals) ;
            mData.setxa((float)(gravSensorVals[0]/9.8));
            mData.setya((float)(gravSensorVals[1]/9.8));
            mData.setza((float)(gravSensorVals[2]/9.8));

            mData.setAkcNonFiltered(event.values.clone());

            linearacc[0] = event.values[0];
            linearacc[1] = event.values[1];
            linearacc[2] = event.values[2];

            linearacc = gravityFilter.gravityFilter(linearacc);
        }
    }

    protected void onPause() {
        mSensorManager.unregisterListener(this);
        super.onPause();
        if(exec !=null)
        {
            exec.shutdown();
        }
    }

    public void saveDrawing(){

        try {
            myView.setDrawingCacheEnabled(true);
            Bitmap bitmap = myView.getDrawingCache();
            File f = null;
            if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
                File file = new File(Environment.getExternalStorageDirectory(),"TTImages_cache");
                if(!file.exists()){
                    file.mkdirs();
                }

                f = new File(file.getAbsolutePath()+file.separator+ "filename"+formattedDate+".png");
            }
            FileOutputStream ostream = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 10, ostream);
            ostream.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
