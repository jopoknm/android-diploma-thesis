package com.example.jozef.gyrouhol;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static com.example.jozef.gyrouhol.R.id.schoolbackround;

public class SchoolMap extends AppCompatActivity implements SensorEventListener, StepListener {

    private SchoolBackground myView2;
    private GestureDetector mDetector;
    private MarkerView marker;
    private OrientationView orientation;
    private RelativeLayout myLayout;
    private boolean overlayActive = false, dataLogin, stepLogin;
    private boolean orientationIsSet = false;
    private float gravSensorVals[];
    private CharSequence markers[], floors[];
    private View inflatedView;
    private ImageButton confirmButton, cancelButton;
    private Button savebutt, addButton;
    private TextView angleinit;
    private Bitmap bitmap, newBitmap;
    private IMUfilter imfilter;
    private GravityFilter gravityFilter;
    private PeakFinder peakFinder;
    private ObjectHandlerNew mData;
    private StepDetector stepDetector;
    private float[] linearacc = {0f, 0f, 0f};
    private double[] angles = {0f, 0f, 0f, 0f};

    private CsvFileWriting csvWriter;
    private double angle;
    private float startingAngle;
    private int count = 0, selectedFloor = 0;
    private int sensorPeriod, stepSize, filterType;
    private MadgwickAHRS madgwickAHRS;

    private SensorManager mSensorManager;
    private Sensor mMagSensor, mAkcSensor, mGyroSensor;

    private ControlPoint controlPoint;
    private File dataWrite, stepWrite;
    private String formattedDate;
    private AlertDialog alertDialog1;
    private ScheduledThreadPoolExecutor exec;

    private DataWorker dataWorker;
    private float axoffset,ayoffset,gxbias,gybias,gzbias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_school);

        myView2 = (com.example.jozef.gyrouhol.SchoolBackground) findViewById(schoolbackround);
        mDetector = new GestureDetector(this, new MyGestureListener());

        controlPoint = new ControlPoint();
        myView2.setOnTouchListener(touchListener);

        myLayout = (RelativeLayout) this.findViewById(R.id.bitmapBox);
        savebutt = (Button) findViewById(R.id.saveButt);
        addButton = (Button) findViewById(R.id.addControlPoint);

        csvWriter = new CsvFileWriting();

        DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;

        myView2.setSize(w, h);

        bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.icon8marker);
        Bitmap bitmapOrientation = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_send_black_24dp);

        marker = new MarkerView(this, bitmap);
        marker.setOnTouchListener(markerTouchListener);

        orientation = new OrientationView(this, bitmapOrientation);
        orientation.setOnTouchListener(orientationTouchListener);

        /** shared prefferences*/
        SharedPreferences shp = PreferenceManager.getDefaultSharedPreferences(this);
        float pedometerMax = Float.valueOf(shp.getString("max_peak", String.valueOf(10.37)));
        float pedometerMin = Float.valueOf(shp.getString("min_peak", String.valueOf(8.5)));
        dataLogin = shp.getBoolean("switch_preference_data_login", false);
        stepLogin = shp.getBoolean("switch_preference_step_login", false);
        sensorPeriod = Integer.parseInt(shp.getString("period_sensor", "20"));
        stepSize = Integer.parseInt(shp.getString("step_size", "15"));
        filterType = Integer.parseInt(shp.getString("filter_type", "1"));
        axoffset = Float.valueOf(shp.getString("axoffset",String.valueOf(0)));
        ayoffset = Float.valueOf(shp.getString("ayoffset",String.valueOf(0)));
        gxbias = Float.valueOf(shp.getString("gxbias",String.valueOf(0)));
        gybias = Float.valueOf(shp.getString("gybias",String.valueOf(0)));
        gzbias = Float.valueOf(shp.getString("gzbias",String.valueOf(0)));

        switch (filterType) {
            case 1:
                float[] q = {1, 0, 0, 0};
                imfilter = new IMUfilter(sensorPeriod * 0.001f, q);
                madgwickAHRS = new MadgwickAHRS(0.005f, 0.5f);
                break;
            case 2:
                madgwickAHRS = new MadgwickAHRS(0.005f, 0.5f);
                break;
        }

        markers = new CharSequence[]{"add marker", "add orientation"};
        floors = new CharSequence[]{"Ground floor", "First floor", "Second floor", "Third floor", "My map"};

        /**calculations */
        gravityFilter = new GravityFilter();
        peakFinder = new PeakFinder();
        mData = new ObjectHandlerNew();
        stepDetector = new StepDetector();
        stepDetector.addStepListener(this);
        stepDetector.setMinMax(pedometerMax, pedometerMin);

        /** Data Login*/
        String deviceName = Build.MODEL;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        formattedDate = df.format(c.getTime());
        if (dataLogin) {
            dataWrite = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/results", deviceName + "_" + formattedDate + ".csv");
        }

        if (stepLogin) {
            stepWrite = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/results", deviceName + "_" + "points_" + formattedDate + ".csv");
        }

        /** sensors registration */
        try {
            mSensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);

            mMagSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mAkcSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

            mSensorManager.registerListener(this, mMagSensor, SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mAkcSensor, SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mGyroSensor, SensorManager.SENSOR_DELAY_FASTEST);
        } catch (Exception e) {
            Toast.makeText(this, "Hardware compatibility issue", Toast.LENGTH_LONG).show();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Set starting position and orientation before moving")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        OnCLickSave();
        OnClickAdd();

        dataWorker = new DataWorker();
    }

    View.OnTouchListener orientationTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (overlayActive) {
                orientation.onTouchEvent(event);
                orientation.invalidate();
                angleinit.setText("" + orientation.getRotation());
                return true;
            }
            return false;
        }


    };

    View.OnTouchListener markerTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (overlayActive) {
                marker.setCurrX(event.getX() - (bitmap.getHeight() / 2));
                marker.setCurrY(event.getY() - (bitmap.getHeight()) + 100);
                marker.invalidate();
                return true;
            }
            return false;
        }
    };


    public void OnCLickSave() {
        savebutt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (controlPoint.getInput() != null) {
                    controlPoint.addFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/results", "_" + "control_points" + formattedDate + ".csv"));
                    controlPoint.writeAll();
                }
                myView2.saveDrawing(formattedDate);
            }
        });
    }

    public void OnClickAdd() {
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                controlPoint.addLine(new String[]{myView2.getLastX() + ";" + myView2.getLastY() + ";" + System.currentTimeMillis() + ";" + "\n"});
            }
        });
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Point p = new Point();
            p.x = myView2.getLastX();
            p.y = myView2.getLastY();
            return mDetector.onTouchEvent(event);
        }
    };

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent event) {
            Log.d("TAG", "onDown: ");

            // don't return false here or else none of the other
            // gestures will work
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.i("TAG", "onSingleTapConfirmed: ");
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            Log.i("TAG", "onLongPress: ");
            marker.setCurrX(e.getX() - (bitmap.getHeight() / 2));
            marker.setCurrY(e.getY() - (bitmap.getHeight()));
            AlertDialog.Builder builder = new AlertDialog.Builder(SchoolMap.this);

            builder.setItems(markers, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                        final LayoutInflater inflater = (LayoutInflater) SchoolMap.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        inflatedView = inflater.inflate(R.layout.marker_overla_layout, null, false);
                        inflatedView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

                        myLayout.addView(marker);
                        myLayout.addView(inflatedView);
                        overlayActive = true;


                        confirmButton = (ImageButton) findViewById(R.id.confirmButton);
                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                AlertDialog.Builder bd = new AlertDialog.Builder(SchoolMap.this);
                                bd.setTitle(R.string.add_marker_confirm)
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                myView2.newStartingPoint((int) (marker.getCurrY() + bitmap.getHeight() + myView2.getcoord().x), (int) marker.getCurrX() + (bitmap.getWidth() / 2) + myView2.getcoord().y);
                                                myView2.invalidate();
                                                myLayout.removeView(inflatedView);
                                                myLayout.removeView(marker);
                                            }
                                        })
                                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {

                                            }
                                        });
                                bd.show();
                            }
                        });

                        cancelButton = (ImageButton) findViewById(R.id.cancelButton);
                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                myLayout.removeView(inflatedView);
                                myLayout.removeView(marker);
                                overlayActive = false;
                            }
                        });
                    }

                    if (which == 1 && myView2.getCircle() != null) {
                        final LayoutInflater inflater = (LayoutInflater) SchoolMap.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        inflatedView = inflater.inflate(R.layout.marker_overla_layout, null, false);
                        inflatedView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));


                        int x = myView2.getCircle().x;
                        int y = myView2.getCircle().y;
                        orientation.setCurrX(myView2.getCircle().x);
                        orientation.setCurrY(myView2.getCircle().y);

                        myLayout.addView(orientation);
                        myLayout.addView(inflatedView);
                        overlayActive = true;

                        angleinit = (TextView) findViewById(R.id.initAngle);

                        confirmButton = (ImageButton) findViewById(R.id.confirmButton);
                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                orientationIsSet = true;
                                exec = new ScheduledThreadPoolExecutor(1);
                                exec.scheduleAtFixedRate(new Runnable() {
                                    private Runnable update = new Runnable() {
                                        @Override
                                        public void run() {


                                            if (filterType == 1) {
                                                imfilter.filterUpdate(mData.getxg(), mData.getyg(), mData.getzg(), mData.getxa(), (float) (mData.getya()), 1);
                                                angle = imfilter.cptangle();
                                            }
                                            if (filterType == 2) {
                                                madgwickAHRS.Update(mData.getxg(), mData.getyg(), mData.getzg(), mData.getxa(), mData.getya(), mData.getza(), mData.getxm(), mData.getym(), mData.getzm());
                                                angle = madgwickAHRS.MadgYaw;
                                            }

                                            stepDetector.stepCount(Math.sqrt(mData.getxa() * mData.getxa() * 9.8 * 9.8 + mData.getya() * mData.getya() * 9.8 * 9.8 + mData.getza() * mData.getza() * 9.8 * 9.8));
                                            peakFinder.addSample(linearacc[2]);

                                            if (dataLogin) {
                                                String[] entryy2 = new String[]{mData.getxg() + ";" + mData.getyg() + ";" + mData.getzg() + ";" + mData.getxa() + ";" + mData.getya() + ";" + mData.getza() + ";" + mData.getxm() + ";" + mData.getym() + ";" + mData.getzm() + ";" + angles[0] + ";" + angles[1] + ";" + angles[2] + ";" + angles[3] + ";" + String.valueOf(System.currentTimeMillis()) + ";" + "\n"};
                                                csvWriter.doWriting(dataWrite,entryy2);
                                            }
                                        }
                                    };

                                    @Override
                                    public void run() {
                                        runOnUiThread(update);
                                    }
                                }, 0, sensorPeriod, TimeUnit.MILLISECONDS);

                                myLayout.removeView(inflatedView);
                                myLayout.removeView(orientation);

                                startingAngle = orientation.getRotation();
                                overlayActive = false;

                            }
                        });

                        cancelButton = (ImageButton) findViewById(R.id.cancelButton);
                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                myLayout.removeView(inflatedView);
                                myLayout.removeView(orientation);
                                overlayActive = false;
                            }
                        });
                    }
                }

            });
            builder.show();
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.i("TAG", "onDoubleTap: ");
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            myView2.notifyScroll(-distanceX, -distanceY);
            myView2.invalidate();
            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            Log.d("TAG", "onFling: ");
            return true;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
    }

    public void stepDetected(StepEvent e) {
        if (orientationIsSet) {
            count++;

            if (filterType == 1) {
                angle = imfilter.cptangle();
            }

            if (filterType == 2) {
                angle = madgwickAHRS.MadgYaw;
            }

            float[] minmax = peakFinder.findMaxfindMin();
            double step = (int) Math.round((dataWorker.getStepLength(minmax[0], minmax[1]) * stepSize));
            myView2.newPointAdd((int) (myView2.getLastX() - (Math.round((step * (Math.cos(angle - Math.toRadians(startingAngle))))))), (int) (myView2.getLastY() - (Math.round(step * (Math.sin(angle - Math.toRadians(startingAngle)))))));

            if (stepLogin) {
                String[] entry = new String[]{myView2.getLastX() + ";" + myView2.getLastY() + ";" + count + ";" + angle + ";" + String.valueOf(System.currentTimeMillis()) + ";" + "\n"};
                csvWriter.doWriting(stepWrite, entry);
            }
        }

    }


    public void onSensorChanged(SensorEvent event) {

        if (event.sensor == mMagSensor) {


            //filteredMag = lowPass(new float[] {(float)(event.values[0]*0.01),(float)(event.values[1]*0.01),(float)(event.values[2]*0.01)}, filteredacc);
            mData.setxm((float) (event.values[0] * 0.01));
            mData.setym((float) (event.values[1] * 0.01));
            mData.setzm((float) (event.values[2] * 0.01));
        }

        if (event.sensor == mGyroSensor) {

            mData.setxg((float) (event.values[0] - gxbias));
            mData.setyg((float) (event.values[1] - gybias));
            mData.setzg((float) (event.values[2] - gzbias));
        }

        if (event.sensor == mAkcSensor) {
            event.values[0] = (float) (event.values[0] - axoffset);
            event.values[1] = (float) (event.values[1] - ayoffset);
            gravSensorVals = dataWorker.doLowPassFiltering(event.values.clone(), gravSensorVals);

            mData.setxa((float) (gravSensorVals[0] / 9.8));
            mData.setya((float) (gravSensorVals[1] / 9.8));
            mData.setza((float) (gravSensorVals[2] / 9.8));

            mData.setAkcNonFiltered(event.values.clone());

            linearacc[0] = event.values[0];
            linearacc[1] = event.values[1];
            linearacc[2] = event.values[2];

            linearacc = gravityFilter.gravityFilter(linearacc);
        }
    }

    protected void onPause() {
        mSensorManager.unregisterListener((SensorEventListener) this);
        super.onPause();
        if (exec != null) {
            exec.shutdown();

        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu_floors, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_floors_settings) {
            ;
            CreateAlertDialogWithRadioButtonGroup();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void CreateAlertDialogWithRadioButtonGroup() {


        AlertDialog.Builder builder = new AlertDialog.Builder(SchoolMap.this);

        builder.setTitle("Choose your floor");

        builder.setSingleChoiceItems(floors, selectedFloor, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        selectedFloor = 0;
                        Toast.makeText(SchoolMap.this, "Prízemie", Toast.LENGTH_LONG).show();
                        myView2.loadNewBackground(newBitmap);
                        break;
                    case 1:
                        selectedFloor = 1;
                        Toast.makeText(SchoolMap.this, "Prvé poschodie", Toast.LENGTH_LONG).show();
                        break;
                    case 2:
                        selectedFloor = 2;
                        Toast.makeText(SchoolMap.this, "Druhé poshcodie", Toast.LENGTH_LONG).show();
                        break;
                    case 3:
                        selectedFloor = 3;
                        Toast.makeText(SchoolMap.this, "Tretie poschodie", Toast.LENGTH_LONG).show();
                        break;
                    case 4:
                        selectedFloor = 4;
                        Toast.makeText(SchoolMap.this, "My Map", Toast.LENGTH_LONG).show();
                }
            }
        });

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                loadNewBitmap(selectedFloor);
                alertDialog1.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    private void loadNewBitmap(int i) {
        switch (i) {
            case 0:
                newBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.prizemie);
                myView2.loadNewBackground(newBitmap);
                break;
            case 1:
                newBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.prve_poschodie);
                myView2.loadNewBackground(newBitmap);
                break;
            case 2:
                newBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.druhe_poschodie);
                myView2.loadNewBackground(newBitmap);
                break;
            case 3:
                newBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.tretie_poschodie);
                myView2.loadNewBackground(newBitmap);
                break;
            case 4:
                openFileDialog();
                break;

        }

    }

    public void openFileDialog() {
        new MaterialFilePicker()
                .withActivity(SchoolMap.this)
                .withRequestCode(1)
                .withFilter(Pattern.compile(".*\\.png$"))
                .withFilterDirectories(false)
                .withHiddenFiles(true)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            Toast.makeText(this, filePath,
                    Toast.LENGTH_LONG).show();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            newBitmap = resize(bitmap, 3141, 1460);
            myView2.loadNewBackground(newBitmap);
        }
    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;

            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

}
