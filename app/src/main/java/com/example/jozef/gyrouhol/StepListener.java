package com.example.jozef.gyrouhol;

/**
 * Created by jozef on 01.03.2018.
 */

public interface StepListener {
     void stepDetected(StepEvent e);
}
