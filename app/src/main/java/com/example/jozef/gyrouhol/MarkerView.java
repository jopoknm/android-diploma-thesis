package com.example.jozef.gyrouhol;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by jozef on 19.03.2018.
 */

public class MarkerView extends View {

    Bitmap bitmap;
    private float currX = 200;
    private float currY = 200;

    public MarkerView(Context context,Bitmap bitmap) {
        super(context);
        this.bitmap = bitmap;

    }

    public float getCurrX() {
        return currX;
    }

    public float getCurrY() {
        return currY;
    }

    public void setCurrX(float currX) {
        this.currX = currX;
    }

    public void setCurrY(float currY) {
        this.currY = currY;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);

        canvas.drawBitmap(bitmap, currX, currY, null);

    }


}


