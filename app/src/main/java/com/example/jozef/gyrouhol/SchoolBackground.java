package com.example.jozef.gyrouhol;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class SchoolBackground extends View {
    private Bitmap bmLargeImage; // bitmap large enough to be scrolled
    private Bitmap largebitmap;

    private Rect displayRect = new Rect(0, 0, 720, 1116); // rect we display to
    private Rect scrollRect = new Rect(0, 0, 720, 1116); // rect we scroll over our bitmap with
    private int scrollRectX = 0; // current left location of scroll rect
    private int scrollRectY = 0; // current top location of scroll rect
    private float scrollByX = 0; // x amount to scroll by
    private float scrollByY = 0; // y amount to scroll by
    private ArrayList<Point> mPoints = new ArrayList<Point>();
    private int width, height;
    private Paint mPaint, mPaintb, mPaintReference, paint, mPaintControl;
    private Paint background;
    private Bitmap marker;
    private boolean startingPointIsSelected =false;
    private Point circle;
    private boolean newPoint = false;
    private int mLastX =0, mLastY= 0;
    private boolean isSizeSet = false;
    public SchoolBackground(Context context, AttributeSet attrs) {
        super(context, attrs);

        largebitmap = BitmapFactory.decodeResource(getResources(),R.drawable.tretie_poschodie);
        marker = BitmapFactory.decodeResource(getResources(),
                R.drawable.icon8marker);
        largebitmap.getHeight();
        largebitmap.getWidth();

        setImage(largebitmap);
        setSize(720,1116);
        setupmPaint();
    }

    public void loadNewBackground(Bitmap bmp){
        largebitmap = bmp;
        marker = BitmapFactory.decodeResource(getResources(),
                R.drawable.icon8marker);
        largebitmap.getHeight();
        largebitmap.getWidth();

        setImage(largebitmap);
        setSize(720,1116);
        setupmPaint();
        invalidate();
    }

    public SchoolBackground(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        largebitmap = BitmapFactory.decodeResource(getResources(),R.drawable.tretie_poschodie);
        setImage(largebitmap);
    }

    public void setSize(int width, int height) {
        background = new Paint();
        background.setColor(Color.WHITE);
        this.width = width;
        this.height = height;

        // Destination rect for our main canvas draw. It never changes.
        displayRect = new Rect(0, 0, width, height);
        // Scroll rect: this will be used to 'scroll around' over the
        // bitmap in memory. Initialize as above.
        scrollRect = new Rect(0, 0, width, height);
        // scrollRect = new Rect(0, 0, bmp.getWidth(), bmp.getHeight());
    }

    public void setImage(Bitmap bmp) {
        if (bmLargeImage != null)
            bmLargeImage.recycle();

        bmLargeImage = bmp;
        scrollRect = new Rect(0, 0, width, height);
        scrollRectX = 0;
        scrollRectY = 0;
        scrollByX = 0;
        scrollByY = 0;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true; // done with this event so consume it
    }

    public void notifyScroll(float distX, float distY) {
        scrollByX = distX; // move update x increment
        scrollByY = distY; // move update y increment
    }

    @Override
    protected void onDraw(Canvas canvas) {


        if(isSizeSet == false){
            int h = canvas.getHeight();
            int w = canvas.getWidth();
            setSize(w,h);
            isSizeSet = true;
        }

        if (bmLargeImage == null)
            return;

        if(!newPoint){
            if (scrollByX != 0 || scrollByY != 0) {
                // Our move updates are calculated in ACTION_MOVE in the opposite     direction
                // from how we want to move the scroll rect. Think of this as
                // dragging to
                // the left being the same as sliding the scroll rect to the right.
                int newScrollRectX = scrollRectX - (int) scrollByX;
                int newScrollRectY = scrollRectY - (int) scrollByY;
                scrollByX = 0;
                scrollByY = 0;

                // Don't scroll off the left or right edges of the bitmap.
                if (newScrollRectX < 0)
                    newScrollRectX = 0;
                else if (newScrollRectX > (bmLargeImage.getWidth() - width))
                    newScrollRectX = (bmLargeImage.getWidth() - width);

                // Don't scroll off the top or bottom edges of the bitmap.
                if (newScrollRectY < 0)
                    newScrollRectY = 0;
                else if (newScrollRectY > (bmLargeImage.getHeight()- height ))
                    newScrollRectY = (bmLargeImage.getHeight() - height);
                scrollRect.set(newScrollRectX, newScrollRectY, newScrollRectX + width
                        , newScrollRectY + height);

                scrollRectX = newScrollRectX;
                scrollRectY = newScrollRectY;
            }
        }
        if(newPoint){
            scrollByX = mPoints.get(mPoints.size()-1).y-width / 2;
            scrollByY = mPoints.get(mPoints.size()-1).x-height / 2;
            int newScrollRectX = (int) scrollByX;
            int newScrollRectY = (int) scrollByY;
            scrollByX = 0;
            scrollByY = 0;

            // Don't scroll off the left or right edges of the bitmap.
            if (newScrollRectX < 0)
                newScrollRectX = 0;
            else if (newScrollRectX > (bmLargeImage.getWidth() - width))
                newScrollRectX = (bmLargeImage.getWidth() - width);

            // Don't scroll off the top or bottom edges of the bitmap.
            if (newScrollRectY < 0)
                newScrollRectY = 0;
            else if (newScrollRectY > (bmLargeImage.getHeight()- height ))
                newScrollRectY = (bmLargeImage.getHeight() - height);
            scrollRect.set(newScrollRectX, newScrollRectY, newScrollRectX + width
                    , newScrollRectY + height);

            scrollRectX = newScrollRectX;
            scrollRectY = newScrollRectY;
            newPoint = false;
        }

        canvas.drawRect(displayRect, background);
        canvas.drawBitmap(bmLargeImage, scrollRect, displayRect, background);

        if(startingPointIsSelected)
        canvas.drawBitmap(marker,mPoints.get(0).y-scrollRect.left-(marker.getWidth()/2),mPoints.get(0).x-scrollRect.top-(marker.getHeight()),null);

        if(mPoints.size()>0) {
            for (int i = 0; i < mPoints.size(); i++) {

                if(i == 0){
                    canvas.drawCircle(mPoints.get(i).y-scrollRect.left, mPoints.get(i).x-scrollRect.top, 5, mPaint);
                    circle = new Point(mPoints.get(i).y-scrollRect.left,mPoints.get(i).x-scrollRect.top);
                }
                if(mPoints.size()>0) {
                    if (mPoints.size() > 1 && i > 0) {
                        if (mPoints.get(i).x > mPoints.get(i - 1).x) {
                            canvas.drawCircle(mPoints.get(i).y-scrollRect.left, mPoints.get(i).x-scrollRect.top, 3, mPaintb);
                            canvas.drawLine(mPoints.get(i - 1).y-scrollRect.left, mPoints.get(i - 1).x-scrollRect.top, mPoints.get(i).y-scrollRect.left, mPoints.get(i).x-scrollRect.top, mPaintb);
                        }

                        if (mPoints.get(i).x < mPoints.get(i - 1).x) {
                            canvas.drawCircle(mPoints.get(i).y-scrollRect.left, mPoints.get(i).x-scrollRect.top, 3, mPaintb);
                            canvas.drawLine(mPoints.get(i - 1).y-scrollRect.left, mPoints.get(i - 1).x-scrollRect.top, mPoints.get(i).y-scrollRect.left, mPoints.get(i).x-scrollRect.top, mPaintb);
                        }

                    } else {
                        canvas.drawCircle(mPoints.get(i).y-scrollRect.left, mPoints.get(i).x-scrollRect.top, 3, mPaintb);
                    }
                }
            }

        }

        if(mPoints.size()!= 0) {
            mLastX = mPoints.get(mPoints.size() - 1).x ;
            mLastY = mPoints.get(mPoints.size() - 1).y ;
        }

    }
    public Point getcoord()
    {
        Point p = new Point(scrollRect.top,scrollRect.left);
        return p;
    }

    public void newPointAdd(int mX, int mY)
    {
        mPoints.add(new Point(mX,mY));
        newPoint = true;
        invalidate();
    }

    public void newStartingPoint(int mX, int mY)
    {
        mPoints.clear();
        mPoints.add(new Point(mX,mY));
        mLastX = mX;
        mLastY = mY;
        startingPointIsSelected = true;
        invalidate();
    }

    public void setupmPaint()
    {
        mPaint = new Paint();
        mPaint.setColor(getResources().getColor(R.color.colorAccentTransparent));
        mPaint.setStrokeWidth(3);

        mPaintb = new Paint();
        mPaintb.setColor(Color.BLUE);
        mPaintb.setStrokeWidth(3);

        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);

        mPaintReference = new Paint ();
        mPaintReference.setColor(Color.rgb(255,0,255));
        mPaintReference.setStrokeWidth(4);

        mPaintControl = new Paint ();
        mPaintControl.setColor(getResources().getColor(R.color.colorReferencePath));
        mPaintControl.setStrokeWidth(3);
    }

    public Point getCircle(){
        return circle;
    }

    public int getLastX()
    {
        return mLastX;
    }
    public int getLastY()
    {
        return mLastY;
    }

    public void saveDrawing(String formattedDate){
        Bitmap mutableBitmap = largebitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas myCanvas = new Canvas(mutableBitmap);
        if(mPoints.size()>0) {
            for (int i = 0; i < mPoints.size(); i++) {

                if(i == 0){
                    myCanvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 5, mPaint);
                    circle = new Point(mPoints.get(i).y,mPoints.get(i).x);
                }
                if(mPoints.size()>1) {
                    if (mPoints.size() > 1 && i > 0) {
                        if (mPoints.get(i).x > mPoints.get(i - 1).x) {
                            myCanvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 3, mPaintb);
                            myCanvas.drawLine(mPoints.get(i - 1).y, mPoints.get(i - 1).x, mPoints.get(i).y, mPoints.get(i).x, mPaintb);
                        }

                        if (mPoints.get(i).x < mPoints.get(i - 1).x) {
                            myCanvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 3, mPaintb);
                            myCanvas.drawLine(mPoints.get(i - 1).y, mPoints.get(i - 1).x, mPoints.get(i).y, mPoints.get(i).x, mPaintb);
                        }

                    } else {
                        myCanvas.drawCircle(mPoints.get(i).y, mPoints.get(i).x, 3, mPaintb);
                    }
                }
            }
        }

        try {
            File f = null;
            if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
                File file = new File(Environment.getExternalStorageDirectory(),"TTImages_cache");
                if(!file.exists()){
                    file.mkdirs();
                }
                f = new File(file.getAbsolutePath()+file.separator+ "filename_"+formattedDate+".png");
            }
            FileOutputStream outputStream = new FileOutputStream(f);
            mutableBitmap.compress(Bitmap.CompressFormat.PNG, 10, outputStream);
            outputStream.close();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}